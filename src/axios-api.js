import axios from 'axios';
import {apiURL} from "./constance";

const instance=axios.create({
    baseURL: apiURL
});

export default instance;