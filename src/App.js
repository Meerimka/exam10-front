import React, {Component, Fragment} from 'react';
import './App.css';
import Toolbar from "./components/UI/Toolbar/Toolbar";
import {Container} from "reactstrap";
import {Route, Switch} from "react-router-dom";
import Posts from "./containers/Posts/Posts";
import NewPost from "./containers/NewPost/NewPost";
import SinglePost from "./containers/SinglePost/SinglePost";


class App extends Component {
    render() {
        return (
            <Fragment>
                <header>
                    <Toolbar/>
                </header>
                <Container style={{marginTop: "20px"}}>
                    <Switch>
                        <Route path="/" exact component={Posts}/>
                        <Route path="/news/new" exact component={NewPost}/>
                        <Route path={"/news/:id"} exact component={SinglePost}/>
                        <Route render={()=><h1>Not Found!</h1>} />
                    </Switch>
                </Container>
            </Fragment>
        );
    }
}

export default App;

