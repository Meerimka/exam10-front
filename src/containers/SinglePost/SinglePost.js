import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {createComment, deleteComment, fetchComments, getSinglePost} from "../../store/action/newsAction";
import {Button, Card, CardBody, Col, Form, FormGroup, Input, Label} from "reactstrap";
import ProductThumbnail from "../../components/ProductThumbnail/ProductThumbnail";



class SinglePost extends Component {

    state={
       author: '',
       comments: ''
    };

    componentDidMount(){

        this.props.fetchComments(this.props.match.params.id);
        this.props.fetchPost(this.props.match.params.id);
    }


    submitFormHandler = event => {
        event.preventDefault();
        this.props.commentCreated({...this.state, news_id: this.props.match.params.id}).then(() =>{
            this.props.history.push('/')
    })
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {
        return (
            <Fragment>
                <div>
                     <h2 style={{margin: "0 auto"}}>{this.props.post.title}</h2>
                        <p>{this.props.post.date}</p>&nbsp;
                        <ProductThumbnail image={this.props.post.image}/>
                        <span>{this.props.post.description}</span>&nbsp;
                </div>

                &nbsp;<h2>Comments</h2>
                {this.props.comments.map(comment =>(
                    <Card key = {comment.id}>
                    <CardBody>
                    <span  style={{marginLeft: "20px"}}><strong style={{textTransform: "uppercase"}}>{comment.author}</strong>&nbsp; wrote:</span>&nbsp;
                    <span>{comment.comments}</span>&nbsp;
                    <Button  className="float-right" color="danger" onClick={()=>this.props.delete(comment.id,comment.news_id)}>Delete</Button>&nbsp;
                    </CardBody>
                    </Card>
                ))}&nbsp;

                &nbsp;<h2>Add Comment</h2>
                <Form onSubmit={this.submitFormHandler}>
                    <FormGroup row>
                        <Label sm={2} for="author">Author</Label>
                        <Col sm={10}>
                            <Input
                                type="text"
                                name="author" id="author"
                                placeholder="Enter your name"
                                value={this.state.author}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2} for="comments">Comments</Label>
                        <Col sm={10}>
                            <Input
                                type="textarea" required
                                name="comments" id="comments"
                                placeholder="Enter your text"
                                value={this.state.comments}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{offset:2, size: 10}}>
                            <Button type="submit" color="primary">Add</Button>
                        </Col>
                    </FormGroup>
                </Form>

            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    post: state.posts.post,
    comments: state.posts.comments
});

const mapDispatchToProps = dispatch =>({
    fetchPost: postId => dispatch(getSinglePost(postId)),
    fetchComments: comId =>dispatch(fetchComments(comId)),
    commentCreated: comData => dispatch(createComment(comData)),
    delete: (comId,post_id) =>dispatch(deleteComment(comId,post_id))
});

export default connect(mapStateToProps,mapDispatchToProps)(SinglePost);