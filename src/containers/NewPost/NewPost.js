import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import NewsPostForm from "../../components/NewsPostForm/NewsPostForm";
import {createPost} from "../../store/action/newsAction";


class NewPost extends Component {


    createPost=newsData =>{
        this.props.postCreated(newsData).then(() =>{
            this.props.history.push('/')
        })
    };

    render() {
        return (
            <Fragment>
                <h2>Add New Post</h2>
                <NewsPostForm onSubmit ={this.createPost}/>
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch =>({
    postCreated: newsData => dispatch(createPost(newsData))
});

export default connect(null,mapDispatchToProps)(NewPost);