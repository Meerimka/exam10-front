import React, {Component, Fragment} from 'react';
import {deletePost, fetchPosts} from "../../store/action/newsAction";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import ProductThumbnail from "../../components/ProductThumbnail/ProductThumbnail";
import {Button, Card, CardBody} from "reactstrap";

class Posts extends Component {

    componentDidMount(){
        this.props.fetchPosts();
    }


    render() {
        return (
            <Fragment>
                <h2>Posts
                    <Link to="/news/new">
                        <Button
                            color="primary"
                            className="float-right">
                            Add new post
                        </Button>
                    </Link>
                </h2>

                {this.props.posts.map(post =>(
                    <Card key = {post.id}  style={{cursor: "pointer"}}>
                        <CardBody>
                                <p  style={{marginLeft: "100px"}}>{post.title}</p>
                            <ProductThumbnail image={post.image}/>
                            <span>{post.date}</span>&nbsp;
                                <Link to={'/news/' + post.id}>
                                <span  style={{marginLeft: "100px"}}>Read Full Post >> </span>
                            </Link>
                            <Button className="float-right"  color="danger" onClick={()=>this.props.delete(post.id)}>Delete</Button>&nbsp;
                        </CardBody>
                    </Card>
                ))}

            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    posts: state.posts.posts
});

const mapDispatchToProps= dispatch =>({
    fetchPosts: () =>dispatch(fetchPosts()),
    delete: (itemId) =>dispatch(deletePost(itemId))
});

export default connect(mapStateToProps,mapDispatchToProps)(Posts);