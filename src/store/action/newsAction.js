import axios from '../../axios-api';

export const FETCH_POSTS_SUCCESS ='FETCH_POSTS_SUCCESS';
export const FETCH_COMMENTS_SUCCESS ='FETCH_COMMENT_SUCCESS';
export const CREATE_POST_SUCCESS ='CREATE_POST_SUCCESS';
export const FETCH_POST_SUCCESS ='CREATE_POST_SUCCESS';
export const FETCH_FAIL ='FETCH_FAIL';

export const fetchPostsSuccess = posts =>({type: FETCH_POSTS_SUCCESS, posts});
export const fetchCommentsSuccess = comments =>({type: FETCH_COMMENTS_SUCCESS, comments});
export const fetchPostSuccess = post =>({type: FETCH_POST_SUCCESS, post});
export const createCommentSuccess = () =>({type: FETCH_POST_SUCCESS});
export const createPostSuccess = () =>({type: CREATE_POST_SUCCESS});
export const fetchFail = error =>({type: FETCH_FAIL});


export const fetchPosts =()=>{
    return dispatch =>{
        return axios.get('/news').then(
            response=> {
                console.log(response.data);
                return dispatch(fetchPostsSuccess(response.data))}
        );
    };
};

export const fetchComments = comId =>{
    return dispatch =>{
        return axios.get('/comments/'+ comId).then(
            response=> {
                console.log(response.data);
                return dispatch(fetchCommentsSuccess(response.data))}
        );
    };
};

export const createPost = newsData => {
    return dispatch => {
        return axios.post('/news', newsData).then(
            () => dispatch(createPostSuccess())
        )
    }
};

export const createComment = comData => {
    return dispatch => {
        return axios.post('/comments', comData).then(
            () => dispatch(createCommentSuccess())
        )
    }
};

export const getSinglePost = (postId) => {
    return dispatch => {
        return axios.get('/news/' + postId).then(
            response => {
                console.log(response.data);
                return dispatch(fetchPostSuccess(response.data));
            },

        );
    }
};

export const deletePost = (newsId) => {
    return dispatch => {
        return axios.delete('/news/' + newsId).then(
            response => {
                dispatch(fetchPosts());
            },
            error => dispatch(fetchFail(error))
        );
    }
};

export const deleteComment = (comId,post_id) => {
    return dispatch => {
        return axios.delete('/comments/' + comId).then(
            response => {
                dispatch(fetchComments(post_id));
            },
            error => dispatch(fetchFail(error))
        );
    }
};


