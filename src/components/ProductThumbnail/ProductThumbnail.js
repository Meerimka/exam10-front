import React from 'react';
import {apiURL} from "../../constance";
import imageNotAvalaible from '../../assets/images/image_not_available.png';


const styles = {
    width: '100px',
    height: '100px',
    marginRight: '10px'
};


const ProductThumbnail = (props) => {
    let image = imageNotAvalaible;

    if(props.image){
       image = apiURL + '/uploads/' + props.image;
    }
    return <img src={image} style={styles} className="img-thumbnail" alt="Post image"/>
};


export default ProductThumbnail;